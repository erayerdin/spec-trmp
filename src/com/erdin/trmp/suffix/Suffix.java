package com.erdin.trmp.suffix;

public class Suffix {
	public String id;
	public String morpheme;
	public String expression;
	
	public Suffix(String expression, String morpheme, String id) {
		this.morpheme = morpheme; this.expression = expression; this.id = id;
	}
}
