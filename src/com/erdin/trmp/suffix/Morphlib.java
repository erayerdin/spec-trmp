package com.erdin.trmp.suffix;

import java.util.ArrayList;
import java.util.List;

public class Morphlib {
	public Suffix[] library;
	
	public Morphlib() {
		List<Suffix> library = new ArrayList<Suffix>();
		
		// 1. Derivational Suffixes
		/*
		String[][] derivational = {
				{"[a-zA-Z]([c�][�iu�])",	"-�I",		"[T�R]"},
				{"[a-zA-Z](l[�iu�]k)", 		"-lIk",		"[T�R]"},
				{"[a-zA-Z](s[�iu�]z)", 		"-sIz",		"[T�R]"},
				{"[a-zA-Z](c[�iu�]l)", 		"-cIl",		"[T�R]"},
				{"[a-zA-Z]([td][ea]�)", 	"-TA�",		"[T�R]"},
				{"[a-zA-Z](ms[�iu�])", 		"-msI",		"[T�R]"},
				{"[a-zA-Z](s[ae]l)", 		"-sAl",		"[T�R]"},
				{"[a-zA-Z](l[ae]n)", 		"-lAn",		"[T�R]"},
				{"[a-zA-Z](l[ae]�)", 		"-lA�",		"[T�R]"},
		};
		*/ // Removed due to algorithmic problems.
		
		// 2. Functional Suffixes
		// 2.1 Conjugations
		Suffix past = new Suffix("[a-zA-Z]([dt][�iu�])([mnk]n?[�i]?z?|l?[ae]?r?)$", "-DI+[UYUM]", "[EY:G�R|ZMN:B�T|GE�]"); library.add(past);
		Suffix evidentiality = new Suffix("[a-zA-Z](m[�iu�]�)(s?�[mnz]�?z?|l?[ae]?r?)$", "-mI�+[UYUM]", "[EY:K�P:TNT]"); library.add(evidentiality);
		Suffix durative = new Suffix("[a-zA-Z]([�iu�]?yor)(s?u[mnz]u?z?|l?a?r?)", "-(i)yor+[UYUM|ZAMAN]", "[EY:G�R:S�R]"); library.add(durative);
		Suffix aorist = new Suffix("[a-zA-Z]([�iu�]r)(s?[�i][mnz][�i]?z?|l?[ae]?r?)$", "-Ir+[UYUM|ZAMAN]", "[EY:K�P:GN�]"); library.add(aorist);
		Suffix modal_future = new Suffix("[a-zA-Z]([ea]c[ea][k�])(s?[�i][mnz][�i]?z?|l?[ae]?r?)", "-AcAk+[UYUM|ZAMAN]", "[EY:K�P:GLC]"); library.add(modal_future);
		Suffix modal_probability = new Suffix("[a-zA-Z]([ea]bilir)(s?i[mnz]i?z?|l?e?r?)", "-Abilir+[UYUM|ZAMAN]", "[EY:K�P:OLS]"); library.add(modal_probability);
		Suffix modal_necessity = new Suffix("[a-zA-Z](m[ea]l[i�])y?(s?[�i][mnz][�i]?z?|l?[ae]?r?)", "-mAlI+[UYUM|ZAMAN]", "[EY:K�P:GRK]"); library.add(modal_necessity);
		Suffix conj_while = new Suffix("ken$", "-ken", "[EY:�KM:S�R]"); library.add(conj_while);
		Suffix conj_if = new Suffix("[a-zA-Z](s[ea])([mnk]n?[�i]?z?|l?[ae]?r?)$", "-sA+[UYUM|ZAMAN]", "[EY:�KM:K�L]"); library.add(conj_if);
		Suffix negation = new Suffix("[a-zA-Z](m[ae])", "-mA+[G�R�N��|UYUM|ZAMAN]", "[EY:MNT:OMS]"); library.add(negation);
		
		// 2.2 Declentions
		Suffix plural = new Suffix("[a-zA-Z'](l[ea]r$)$", "-lAr", "[AD:�O�UL]"); library.add(plural);
		Suffix determiner = new Suffix("[a-zA-Z'](n?[�iu�])$", "-(y)I", "[AD:BEL]"); library.add(determiner);
		Suffix place = new Suffix("[a-zA-Z']([td][ae]n)$", "-DAn", "[AD:YER]"); library.add(place);
		Suffix source = new Suffix("[a-zA-Z']([td][ae])$", "-DA", "[AD:BUL]"); library.add(source);
		Suffix goal = new Suffix("[a-zA-Z'](y?[ae])$", "-(y)A", "[AD:Y�N]"); library.add(goal);
		Suffix genitive = new Suffix("[a-zA-Z'](n?[�iu�]n)$", "-(n)In", "[AD:TAM]"); library.add(genitive);
		Suffix possesive = new Suffix("[a-zA-Z'](s?[�iu�])?n[�iu�]$", "-(s)I", "[AD:�YE]"); library.add(possesive);
		Suffix possesive_1pl = new Suffix("[a-zA-Z]([�iu�]?m[�iu�]z)$", "-(I)mIz", "[AD:�YE:1��]"); library.add(possesive_1pl);
		Suffix possesive_2pl = new Suffix("[a-zA-Z]([�iu�]?n[�iu�]z)$", "-(I)nIz", "[AD:�YE:2��]"); library.add(possesive_2pl);
		
		
		/*
		for (String[] i : derivational) {
			library.add(new Suffix(i[0], i[1], i[2]));
		}
		*/
		
		this.library = library.toArray(new Suffix[library.size()]);
	}
}
