package com.erdin.trmp.test;

import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.erdin.trmp.Utterance;
import com.erdin.trmp.suffix.Morphlib;
import com.erdin.trmp.suffix.Suffix;

public class App {
	public static void main(String[] args) {
		
		String[] utts = {
			"Mehmet eve kitap getirdi.",
			"Ye�im bakkaldan ekmek sat�n ald�.", // Olmad�.
			"Bu �ocuk her sabah s�t i�iyor.", // Olmad�.
			"Her sabah parkta y�r�y�� yap�yorum.", // Olmad�.
			"Kedicik sokakta dondu.",
			"Melih eve ekmek getirdi.",
			
			/*
			 * Regex'te Unicode karakterleri match eden bir�ok
			 * d�k�mana g�z atsam da bulamad�m. Bu sebeple �rnek
			 * t�mcelerin ASCII karakterlerle sunulmas� gerekli.
			 * Buna (muhtemelen) bir fonksiyon yaz�lacak.
			 * 
			 * Not: Fonksiyon i�e yaramad�. Ascii girilmeli.
			 */
			
			"Yesim bakkaldan ekmek satin aldi.",
			"Bu cocuk her sabah sut iciyor.", // �ok az hata.
			"Her sabah parkta yuruyus yapiyor.", // -um olmay�nca i�e yar�yor.
			"Neler konusuluyor orada oyle?" // Hatalar var.
		};
		
		/*
		for (String i : utts) {
			String filler = "#";
			for (int j = 0 ; j < i.length()-1 ; j++) {filler += "#";}
			System.out.println(filler);
			System.out.println(i);
			System.out.println(filler);
			Utterance utt = new Utterance(i);
			Map<String, String[]> a = utt.get_allomorphs();
			Iterator<String> keySetIterator = a.keySet().iterator();
			
			while (keySetIterator.hasNext()) {
				String key = keySetIterator.next();
				System.out.println(key);
				System.out.println("------------");
				
				for (String q : a.get(key)) {
					System.out.println(q);
				}
				System.out.println("");
			}
		}
		*/
		
		// K�kleri Alma Testi
		for (String i : utts) {
			String filler = "#";
			for (int j = 0 ; j < i.length()-1 ; j++) {filler += "#";}
			System.out.println(filler);
			System.out.println(i);
			System.out.println(filler);
			Utterance utt = new Utterance(i);
			String[] stems = utt.get_stems();
			
			for (String q : stems) {
				System.out.println(q);
			}
		}
		
		System.exit(0);
	}
}
