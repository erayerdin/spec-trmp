package com.erdin.trmp;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.erdin.trmp.suffix.Morphlib;
import com.erdin.trmp.suffix.Suffix;

public class Utterance {
	protected String utterance;
	
	public Utterance(String utterance) {
		this.utterance = Normalizer.normalize(utterance, Normalizer.Form.NFKD);
	}
	
	private String[] tokenize() {
		Pattern pattern = Pattern.compile("[a-zA-Z]+");
		Matcher matcher = pattern.matcher(this.utterance);
		
		List<String> tokens = new ArrayList<String>();
		
		while (matcher.find()) {
			tokens.add(matcher.group(0));
		}
		
		String[] tokenbox = tokens.toArray(new String[tokens.size()]);
		
		return tokenbox;
	}
	
	public String parser() {
		
		return null;
	}
	
	public Map<String, String[]> get_allomorphs() {
		Morphlib lib = new Morphlib();
		String[] tokens = this.tokenize(); // T�mce tokenize (s�zc�k-s�zc�k) ediliyor.
		Map<String, String[]> allomorphs = new HashMap<String, String[]>();
		
		for (String token : tokens) {
			// token'de bulunan e�le�meleri koymak i�in bir liste olu�turuluyor.
			List<String> found = new ArrayList<String>();
			
			for (Suffix suffix : lib.library) {
				// Son ek k�t�phanesinin "library" de�i�kenindeki t�m "Suffix" nesneleri teker teker al�n�yor.
				Pattern pattern = Pattern.compile(suffix.expression, Pattern.UNICODE_CHARACTER_CLASS | Pattern.UNICODE_CASE); // �lgili regex'i kullan.
				Matcher matcher = pattern.matcher(token); // �lgili t�rcede (token) regex'i ara.
				
				try {
					while (matcher.find()) { // sonu�lar� ele alan d�ng�. sonu� yoksa hata verir. ==> <a>
						for (int i = 1 ; i < 3 ; i++) { // 1-2 aras� say�lar� d�ng� olarak ver. <b>
							try {
								found.add(matcher.group(i)); // "found" listesine ilgili e�le�meyi yerle�tir
								// Baz� ifadeler ikili gruplar ele al�yor.
								// �rne�in �u ifade: "[a-zA-Z]([ea]c[ea][k�])(s?[�i][mnz][�i]?z?|l?[ae]?r?)"
								// iki ek d�nd�r�r: -ecek -im
								// Ama baz�lar� sadece bir tane grup ele alacak:
								// �rne�in �u ifade: [a-zA-Z'](l[ea]r$)$
								// sadece bir ek d�nd�r�r: -lar
							} catch (Exception e) {break;} // e�er ikinci grup yoksa <b> d�ng�y� sonland�r
						}
					}
				} catch (Exception e) {
					continue; // e�er sonu� yoksa <a> d�ng�y� sonland�r.
				}
			}
			
			allomorphs.put(token, found.toArray(new String[found.size()]));
			// Sonu�lar� ekler.
			// Hangi t�rcelerin hangi eklere sahip oldu�unu anlamak i�in
			// hashmap kullan�ld�. �rne�in "Ben geldim." t�mcesinde
			// [geldim] t�mcesi �u �ekilde d�ner:
			// String "geldim" : String[-di, -m]
		}
		
		return allomorphs;
	}
	
	public String[] get_stems() {
		/*
		 * Bu fonksiyonun mant��� �udur:
		 * 1) T�rceleri elde et.
		 * 2) Ekleri ele al.
		 * 3) Her t�rceyi dola�.
		 * 4) E�er t�rcede ek varsa sil.
		 * 5) T�m t�rcelerde bunu devam ettir.
		 * 6) Array olarak geri d�nd�r.
		 */
		String[] tokens = this.tokenize(); // S�zc�k s�zc�k ay�r.
		Map<String, String[]> allomorphs = this.get_allomorphs(); // Ekleri ele al.
		List<String> stems = new ArrayList<String>();
		
		for (String token : tokens) { // Her bir t�rceyi ele al.
			try {
				String[] suffixes = allomorphs.get(token); // �lgili t�rcede bulunan ekleri ele al.
				Pattern p = Pattern.compile(suffixes[0]+"$", Pattern.UNICODE_CHARACTER_CLASS);
				Matcher m = p.matcher(token);
				String newToken = m.replaceFirst(""); // �lk eki sil.
				stems.add(newToken); // Ve kalan� da cehenneme g�nder.
			} catch (Exception e) {
				stems.add(token); // E�er ek bulunamam��sa do�rudan k�k� ekle.
			}
		}
		
		return stems.toArray(new String[stems.size()]);
	}
}
